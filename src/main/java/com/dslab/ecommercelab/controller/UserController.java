package com.dslab.ecommercelab.controller;

import com.dslab.ecommercelab.entity.User;
import com.dslab.ecommercelab.entity.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    UserRepository repository;

    //POST http://localhost:8080/user/register
    @PostMapping(path = "register")
    public @ResponseBody User register(@RequestBody User user) {
        user.setRoles(Collections.singletonList("USER"));
        return repository.save(user);
    }

    //GET http://localhost:8080/user/all
    @GetMapping(path = "/all")
    public @ResponseBody Iterable<User> getAll() {
        return repository.findAll();
    }

    //GET http://localhost:8080/user/1
    @GetMapping(path = "/{id}")
    public @ResponseBody User getUser(@PathVariable Integer id) {

        Optional<User> byId = repository.findById(id);
        return byId.get();
    }

    //DELETE http://localhost:8080/user/1
    @DeleteMapping(path = "/{id}")
    public @ResponseBody String deleteUser (@PathVariable Integer id) {
        repository.deleteById(id);
        return String.format("The user with id %d has been deleted!", id);
    }
}
